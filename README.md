## ResetFlake

An Arduino Sketch that resets a modem, router, or other device when 
the Internet is not reachable, and shows uptime and outage
stats as a web page and REST service. Enters captive portal
WiFi configuration mode at boot if the configured WiFi is not available.

For the WeMos D1 Mini or compatible board. 

Should be fairly portable to any ESP8266 setup, with some configuration.
Note that I am using the NC (normally closed) pins on a relay attached
to D1. You could just as easily use a MOSFET, reverse the HIGH/LOW state 
of D1, and switch the ground lead of the DC power. Etc. Or whatever.

It also is a decent example of how to add captive portal configuration
mode to an ESP8266WebServer-based device (see IotPortalHelper.cpp, start(), loop(), and
the 404 web handler).

### Disclaimer

I'm switching the +12V supply with a relay hat. If you electrocute yourself trying to 
switch mains power, and you survive, don't come crying to me.

### Requires

  * ArduinoJson
  * ESP8266Wifi (contrib 1.0.0 tested)
  * ESP8266WebServer (1.0.0 tested)
  * ESP8266Ping (https://github.com/dancol90/ESP8266Ping)
  * Arduino ESP8266 filesystem uploader plugin, or any other SPIFFS data writing method
  * Time by Michael Margolis (1.5.0 tested)
  
### Getting Started

#### 1. Compile/upload the sketch and run it.

I assume you know how to do this. :)

#### 2. Compile the web page

To build, run "make web-ui" You will need yarn installed in the path. Yarn will first download about
five million node packages, then build the web site. The makefile will then
copy it to data/web/. Note that anything you add to data/web will be removed on every build!

#### 2. Upload the SPIFFS image

The web page and configuration are stored in the SPIFFS data, aka the "sketch data". Upload
it using your favorite tool. 

I'm using the Arduino environment.
I use the [ESP8266 Sketch Data Upload](https://github.com/esp8266/arduino-esp8266fs-plugin)
plugin to upload the static web files.

#### 3. Boot into AP mode and configure

Power up ResetFlake. It may try to connect to the last configured AP - the light will
stay steady on while it tries to connect (or when it is pinging). After 30 seconds it
will give up trying to connect, and enter AP mode. At this point the light will slowly
flash. Connect to the "ResetFlake" network using the super
secrete password: "password". Browse to 192.168.1.1 and 
you should see the configuration screen. (Note: If you do not connect within 5 minutes,
ResetFlake will reboot to try to connect to WiFi again. If  you load the config
page, this will cancel the automatic reboot.)

Choose an IP and hostname to ping. I recommend choosing hosts that that are directly
upstream on your ISP. If your host has a reliable primary DNS resolver, this is a good
choice. CenturyLinkTM's DNS server is tragically overloaded, so I use
speedtest.centurylink.com. As it turns out, IPSs have a vested interest in keeping their
speedtest hosts online and responsive, so this is a great solution. Or you can use
the old standby google, but then your modem will reset if the route to google
goes down, even if the rest of the internet is working fine. So don't do that.

Saving the configuration will reboot ResetFlake.

#### 4. Browse to ResetFlake to view stats

First, reserve an IP for ResetFlake on your router using its DHCP reservation
feature. If your router does not have this feature, add static IP configuration features
to this project and submit a PR.

Now browse to ResetFlake's IP address to see some simple stats. 

Stats are available as the default page. REST apps can browse to /api/stats
to get all stats as JSON. There is also a handy "Hello, world." rest service
located at "/api/hello", and the current ping configuration is available from "/api/config".

### License

Licensed under the [WTFPL](http://www.wtfpl.net), the openest of all open source licenses,
if possibly the least revolutionary. With great power comes great responsibility: no
warranty of any kind is implied.
