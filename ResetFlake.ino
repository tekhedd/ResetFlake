// 
// ResetFlake - it resets your flaky internet connectin.
//
// Written for ESP8266 (WeMos D1 Mini) 
// with Relay Shield, or anything switched via pin D1.
//

#include <ArduinoJson.h>
#include <DNSServer.h>
#include <FS.h>
#include <Time.h>
#include <TimeLib.h>

#include <ESP8266WiFi.h> // default contrib v1.0.0, or from git.
#include <ESP8266WebServer.h>

// See https://github.com/dancol90/ESP8266Ping
#include <ESP8266Ping.h>

// Local classes

#include "./IosPortalHelper.h"
#include "./Stats.h"

//
// -- Configuration
//

//
// Goal: reset the fragile C1000A modem when my upstream link dies.
//
// Use both host name and IP to check that dns and routing are working. 
// Do not 100% trust hostname-based ping, because the default router can 
// return its own IP for "magic autoconfig", resulting in a successful ping
// but not to the host you think you reached. :o
//
// That said, we *do* need to confirm that DNS is working, so do a DNS lookup.
// No need to ping the host re
//
// I initially used CenturyLinkTM's DNS server name/IP, because that seemed logical:
// sometimes DNS stops working at CenturyLink and will not start again until the
// modem is reset. However, CenturyLink's DNS server is massively overloaded and
// frequently does not respond to ping. This made my connection even LESS reliable.
// So, here let's try pinging centurylink's main web site. Surely they will at least
// make an effort to make that seem reliable, yes?
//

//
// -- Web Config
//

///
/// Host to ping. Loaded from config.json
///
static String pingHost( "speedtest.centurylink.com" );

///
/// IP to ping. Also loaded from config.json
///
static IPAddress pingIp( 8, 8, 8, 8 ); // google dns? please override this in config

//
// -- Advanced / Internal configuration
//

#define RELAY_PIN (D1)

// 
// Define this to make all the timeouts shorter for debugging.
//
// #define IMPATIENT_DEBUG_MODE 1

// Default interval at which to ping servers
#ifdef IMPATIENT_DEBUG_MODE
#define DEFAULT_POLL_S (15)
#else
#define DEFAULT_POLL_S (90)
#endif

//
// Time to wait after a reset
// Initial wait time is MS + INCREMENT
//
// CenturyLink's docs recommend waiting, what, 2 minutes?
//
// VDSL2 takes literally forever (OK, really about 4 minutes) to renegotiate and
// authenticate.
//
#define RETRY_WAIT_S (240)

#ifdef IMPATIENT_DEBUG_MODE
#define RETRY_WAIT_INCREMENT_S (45)
#else
#define RETRY_WAIT_INCREMENT_S (5)
#endif

// Time to stay off when power cycling
#define STAY_OFF_MS (5000)

// Additional time to stay off on subsequent retries
#define STAY_OFF_INCREMENT (1000)

//
// Ping config: A reset takes >90s, and then everything downstream
// of the modem has to reconfigure. Sometimes the network recovers.
// How long do we keep trying a failed ping before we give up and
// reset the internets? Let's be very patient, and wait for at least
// the length of a pop song.
//
#define PING_RETRY_MS (180000)

//
// Time to wait between pings, when the first one fails
//
#define PING_DELAY_MS (2000)

//
// Blink rate in AP mode
//
#define AP_BLINK_S (2)

//
// -- Internal state
//

///
/// Internal reset counter -- this does not keep counting past 5-ish.
///
/// How many consecutive times have we tried a reset and failed?
/// 
/// (Should be less than 5 or so. Past that we stop counting and therefore
/// also stop increasing wait times etc.)
///
static int resetCount = 0;

///
/// Normally set to false in AP mode. If false, ping/reset engine is disabled
///
static bool isApMode = true;

///
/// Used to blink the LED in ap mode. You know.
///
static uint8_t apLedBlinkState = LOW;

///
/// In AP mode only: if this is > 0, the main loop will reboot when the system has been running
/// for this long (in other words if millis() is greater). This allows us to recover
/// rather than getting stuck in AP mode if WiFi was for some reason temporarily
/// unavailable. And yes we'll go into a perpetual (if slow) boot loop if the WiFi is
/// down. Too bad!
///
/// And yeah, millis() wraps, but I'm assuming we will reboot after minutes, not days
/// here.
///
static unsigned long forceRebootMillis = 0;

#define BOOT_WIFI_TIMEOUT (30000) // = 30s
#define AP_MODE_REBOOT_MILLIS (300000) // = 300s = 5 minutes

static Stats stats;
static ESP8266WebServer server(80);
static DNSServer dnsServer;
static IosPortalHelper portalHelper(dnsServer);

void setup() 
{
   pinMode(LED_BUILTIN, OUTPUT);
   pinMode(RELAY_PIN, OUTPUT);
 
   Serial.begin(230400); // 115200);
   
   while(!Serial) {} // Wait for console log
   Serial.println("ResetFlake: serial debug initialized");
 
   digitalWrite(RELAY_PIN, LOW);    // Power on by default (relay off)
   
   // The ESP8266 LED HIGH = off
   digitalWrite(LED_BUILTIN, LOW); // Light up while attempting to connect
 
   // Serve web pages and get config from the filesystem.
   if (!SPIFFS.begin()) {
      Serial.println("SPIFFS init failed");
   }
 
   loadConfig();
   
   Serial.print("Connecting to wifi: "); Serial.println(WiFi.SSID());
   if (IosPortalHelper::tryStartClient(BOOT_WIFI_TIMEOUT))
   {
      Serial.println("WiFi connected");
      isApMode = false;
      forceRebootMillis = 0; // don't reboot, it's working! It's workiiing!
   }
   else
   {
      Serial.println("AP mode");
      isApMode = true;
      forceRebootMillis = AP_MODE_REBOOT_MILLIS; // Don't get stuck in AP mode
      portalHelper.startAP("ResetFlake", "password");
   }
  
   digitalWrite(LED_BUILTIN, HIGH); // Lights out
   
   _addWebHandlers();
   server.begin();  
}

void loop() 
{
   int sleepytime; // seconds
   if (isApMode) // Don't ping in AP mode
   {
      unsigned long now = millis();
      
      // Don't get stuck in AP mode if nobody is trying to configure it
      if (forceRebootMillis > 0 && now > forceRebootMillis)
      {
         Serial.println("No web activity detected, rebooting/retrying.");
         ESP.restart();
      }
      
      // While in AP mode, blink so you'll know it's config time!
      apLedBlinkState = ! apLedBlinkState; // yes this is bad style but SO EASY
      digitalWrite(LED_BUILTIN, apLedBlinkState);

      sleepytime = AP_BLINK_S;
   }
   else
   {
      sleepytime = wakeUpAndPing();
   }
      
   Serial.print("zzz... (");
   Serial.print(sleepytime);
   Serial.println("s)");
   delayAndServe(sleepytime * 1000);
}

///
/// Called if any web handlers associated with configuration get called, to
/// extend or cancel the automatic reboot time.
///
void webActivity()
{
   // Cancel automatic reboot
   if (forceRebootMillis > 0)
   {
      Serial.println("Cancelling automatic reboot");
      forceRebootMillis = 0;
   }
}

void _addWebHandlers()
{
   server.on("/api/stats", HTTP_GET, [&]() {
      webActivity();
      server.send( 200, "application/json", stats.asJson());
   });
   
   server.on("/api/wifi/scan", HTTP_GET, [&]() {
      webActivity();
      server.send( 200, "application/json", IosPortalHelper::wifiScan() );
   });
 
   server.on("/api/config", HTTP_GET, [&]() {
      // Once loaded, the client's first action is to load the config, so cancel
      // the reboot here:
      webActivity();
      server.send( 200, "application/json", getConfig() );
   });
   
   // save config AND REBOOT
   server.on("/api/config", HTTP_POST, [&]() {
      webActivity();
      
      // The magic "plain" argument contains the entire request body
      DynamicJsonBuffer jsonBuffer; // or Static<> or Dynamic
      JsonObject& json = jsonBuffer.parseObject( server.arg("plain"));

      // Only overwrite things that aren't empty
      String arg = json.get<String>("pingHost");
      if (arg.length() > 0)
      {
         pingHost = arg;
         Serial.print("pingHost updated: "); Serial.println(arg);
      }

      arg = json.get<String>("pingIp");
      if (arg.length() > 0)
      {
         IPAddress newIp;
         if (!newIp.fromString(arg))
         {
            server.send( 400, "Can not parse Ping IP" );
            return;
         }
         
         pingIp = newIp;
         Serial.print("pingIP updated: "); Serial.println(arg);
      }
      
      bool ssidChanged = false;
      String clientSsid = WiFi.SSID();
      arg = json.get<String>("clientSsid");
      if (arg.length() > 0 && clientSsid != arg) // if the ssid changed...
      {
         ssidChanged = true;
         clientSsid = arg;
         Serial.print("clientSsid updated: "); Serial.println(arg);
      }
      
      String password = json.get<String>("clientPassword");
         
      saveConfig(); // Save everything except WiFi
      
      // Save wifi ssid/pass in the ESP8266 itself. Can we do this?
      if (password.length() > 0)
      {
         Serial.println("password: <changed>");
         WiFi.begin(clientSsid.c_str(), password.c_str());  // new password
      }
      else if (ssidChanged)
      {
         WiFi.begin(clientSsid.c_str());            // same password, new ssid
      }
         
      server.send( 200 );

      // And now we reboot

      delay(250);
      Serial.println("ESP.restart()");      
      ESP.restart(); // This is how we're supposed to do it
      delay(5000);
      Serial.println("ESP.restart() failed; trying reset()");
      ESP.reset();   // If all else fails, do a hard boot
      delay(10000);
      Serial.println("ESP.reset() failed too; trying despair");
   });
   
   server.on("/api/hello", HTTP_GET, [&]() {
      webActivity();
      server.send( 200, "text/plain", "Hello, world");
   });
   
   server.on("/", HTTP_GET, redirectToIndex);
   
   // Shouldn't need these in AP mode if we're serving the index for all 404s
   //server.on("/generate_204", serveIndex);  // android
   //server.on("/fwlink", serveIndex); 
   
   server.serveStatic("/", SPIFFS, "/web/"); 
   
   // Enthusiastically edirect "not found" to the main config page if in AP mode
   // Coincidentally also an example of how to handle HTML5-navigation refresh loading
   server.onNotFound([&]() {
      Serial.print("404: "); Serial.println(server.uri());
         
      if (isApMode)
      {
         redirectToIndex();
         return;
      }
      
      server.send(404, "text/plain", "404 not found");
   });
}

void redirectToIndex()
{
   webActivity();
   
   Serial.print("sending redirect from: "); Serial.println(server.uri());
   
   String location;
   if (isApMode)
   {
      // redirect to the full url with IP address, to make the point unambiguously
      location = "http://" + portalHelper.getApIp().toString() + "/index.html";
   }
   else
   {
      location = "/index.html";
   }
   server.sendHeader("Location", location);
   server.send(302, "text/plain", "Redirecting to index...");
}

/////
///// Handler that redirects the root to index.html (and all pages when in
///// AP/captive portal mode for config.)
/////
//void serveIndex()
//{
//   File file = SPIFFS.open("/web/index.html", "r");
//   if (!file)
//   {
//      server.send(500, "index.html could not be read!");
//      return;
//   }
//
//   server.streamFile(file, "text/html");
//}

///
/// Serve the current config just in case you forgot what it was. :)
///
String getConfig()
{
  String json = "{\n";
  
  json += "\"apMode\":";
  json += isApMode ? "true" : "false";
  json += ",\n";

  json += "\"clientSsid\":\"";
  json += WiFi.SSID();
  json += "\",\n";
  
  // We don't send clientPassword, we just receive it
  json += "\"clientPassword\":\"\",\n";
  
  json += "\"pingHost\":\"";
  json += pingHost;
  json += "\",\n";
  
  json += "\"pingIp\":\"";
  json += pingIp.toString();
  json += "\"\n";
  
  // TODO: configurable ping timeouts & reset time
  
  json += "}";
  return json;
}

// -- Main ping program --

///
/// @returns time to wait in seconds
///
int wakeUpAndPing()
{
  // Turn the led on while checking, and turn it off on success.
  // If the light stays on, we are in trouble. :o
  
  digitalWrite(LED_BUILTIN, LOW);
  bool pingStatus = doPing();
  if (pingStatus)
  {
    digitalWrite(LED_BUILTIN, HIGH); // success, turn off LED!
    resetCount = 0;
  }
  else
  {
    ++ resetCount;
    if (resetCount > 5) // limit max poll delay if we're offline
      resetCount = 5;
      
    doReset();
  }

  // stats.printToSerial();
  
  if (resetCount <= 0)
    return DEFAULT_POLL_S;
  else
    return RETRY_WAIT_S + (resetCount * RETRY_WAIT_INCREMENT_S);
}

bool doPing()
{
  // The net is up if we can ping our upstream IP (or something nearby), 
  // and also DNS is working. 
  //
  // Reference config: Centurylink's DNS sometimes fails until we
  // reset the modem, even though IP routing still works. :(

  // If we can't get both our pings within a set time, it failed
  unsigned long startTime = millis();

  if (!persistentPing(pingIp, startTime))
    return false;
    
  // And now by hostname

  Serial.print("Ping host: ");
  Serial.println(pingHost);
  
  IPAddress byNameIp;
  if (!WiFi.hostByName(pingHost.c_str(), byNameIp))
    return false;

  if (!persistentPing(byNameIp, startTime))
    return false;
  
  return true;
}

///
/// Ping the address repeatedly until one successful ping happens
/// or we reach PING_RETRY_MS, assuming we started at time startMillis
///
bool persistentPing(IPAddress dest, long startMillis)
{
  Serial.print("Pinging ");
  Serial.print(dest);
  Serial.print("..");

  while (true)
  {
    if (Ping.ping(dest, 1)) 
    {
      int avg_time_ms = Ping.averageTime();
      Serial.print(avg_time_ms);
      Serial.println("ms");
      stats.logPingTime(avg_time_ms);
      
      return true;
    }

    stats.logPingFail(); // If we got here, ping failed. Noooo!
    
    if ((millis() - startMillis) > PING_RETRY_MS) // timed out?
    {
      Serial.println("fail. :(");
      return false;
    }
    else
    {
      Serial.print(".");
      delayAndServe(1000);
    }
  }
}

void doReset()
{
  stats.logReset();
  
  Serial.print("Resetting...");
  digitalWrite(RELAY_PIN, HIGH); // energize to disconnect: using normally-closed contacts
  delayAndServe(STAY_OFF_MS + (resetCount * STAY_OFF_INCREMENT));
  digitalWrite(RELAY_PIN, LOW);
  Serial.println("done");
  
  stats.logResetWait(); // Now we wait, and wait, and wait.
}

///
/// Delay, but also serve internet clients.
///
void delayAndServe(long milliseconds)
{
   unsigned long start = millis();
   
   while (true)
   {
      long howlong = millis() - start;
      if (howlong >= milliseconds)
         break;
      
      dnsServer.processNextRequest();
      server.handleClient();
      
      if (milliseconds > 10)
         delay(10);
      else
         delay(milliseconds);
   }
}

///
/// Load settings from spiffs config
///
void loadConfig()
{
   // Read json config if found
   File configFile = SPIFFS.open("/config.json", "r");
   if (!configFile)
   {
      Serial.println("ERROR: config.json not found; using defaults");
   }
   else
   {
      // Warning: parse() copies the entire file into memory.
      DynamicJsonBuffer jsonBuffer; // or Static<> or Dynamic
      JsonObject & root =  jsonBuffer.parse(configFile);
      configFile.close();
      
      if (!root.success())
      {
         Serial.println("Could not parse config.json; using defaults");
      }
      else
      {
         // root.printTo(Serial);
         pingHost = (const char *)root["pingHost"];
         pingIp.fromString( (const char *)root["pingIp"] );
      }
   }
}


void saveConfig()
{
   Serial.println("saveConfig()");
   
   DynamicJsonBuffer jsonBuffer;
   JsonObject& json = jsonBuffer.createObject();
   json["pingHost"] = pingHost;
   json["pingIp"] = pingIp.toString();
   json.prettyPrintTo(Serial);

   File configFile = SPIFFS.open("/config.json", "w");
   if (!configFile) {
      Serial.println("Could not open config.json for writing");
      return;
   }

   json.printTo(configFile);
   configFile.close();
}
