const path = require("path");
const webpack = require("webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

// Point this to whatever address the esp8266 is running for live testing
// Trailing slash required
const PROXY_ROOT = "http://10.1.1.139/";

module.exports = {
   entry: path.resolve(__dirname, "src/index.js"),
   mode: "development",
   output: {
      path: path.resolve(__dirname, "output/"),
      publicPath: "",
      filename: "bundle.js"
   },
   resolve: {
      extensions: ["*", ".js", ".jsx"]
   },
   module: {
      rules: [
         // UglifyJS lets us save copyright headers in a separate file, so we don't
         // need this. 
         // webpack's built-in minizer, includes comments in the bundle (yuck) so if
         // we go back to that we'll need this.
         //{
         //   test: /\.(js|css)$/,
         //   loader: "stripcomment-loader",
         //   enforce: "pre"
         //},
         {
            test: /\.js$/,
            loader: "babel-loader",
            options: {
               presets: ["@babel/env"]
            }
         },
         {
            test: /\.css$/,
            use: ["style-loader", "css-loader"]
         },
         {
            test: /\.scss$/,
            use: [
               process.env.NODE_ENV !== 'production' ? 'style-loader' : MiniCssExtractPlugin.loader,
               'css-loader',
               'sass-loader'
            ]
         }
      ]
   },
   devtool: 'source-map',
   devServer: {
      contentBase: path.resolve(__dirname, 'public'),
      port: 3000,
      publicPath: '',
      hot: false,
      proxy: {
         "/wifi/*": PROXY_ROOT ,
         "/api/*": PROXY_ROOT
      }
   },
   optimization: {
      minimizer: [
         new UglifyJSPlugin({
            sourceMap: true,
            uglifyOptions: {
               warnings: false,
               output: {
                  comments: false
               },
               mangle: true  // Note `mangle.properties` is `false` by default.
            }
            // So, what exactly do we DO with all this license data?
            // extractComments: true
         })
      ]
   },
   plugins: [
      new MiniCssExtractPlugin({
         // Options similar to the same options in webpackOptions.output
         // both options are optional
         filename: "[name].css",
         chunkFilename: "[id].css"
      })
   ]
};
