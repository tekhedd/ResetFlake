// @flow

type OutageHistoryElement = {
   ago: number, // sec
   length: number
};

export class StatusData {
   status: 'OK'|'DOWN'|'RESETTING'|'RESET_WAIT'|'UNKNOWN';
   uptime: number;         // sec
   outageDuration: number; // sec
   linkUp: number;
   outageMax: number;
   outageAvg: number;
   resetCount: number;
   ping: number; // ms
   pingAvg: number; // ms
   pingMax: number; //ms
   outageHistory: Array<OutageHistoryElement>;
   
   constructor() {
      this.status = 'UNKNOWN';
      this.uptime = 0;
      this.outageDuration = 0;
      this.linkUp = 0;
      this.outageMax = 0;
      this.outageAvg = 0;
      this.resetCount = 0;
      this.ping = 0;
      this.pingAvg = 0;
      this.pingMax = 0;
      this.outageHistory = [];
   }
}


