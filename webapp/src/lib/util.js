// @flow
//
// util.js -  Just simple utility functions
//

/**
 * Format duration as 'days hours:minutes:seconds'
 *
 * @param seconds duration in seconds
 */
export function dhms(seconds : number) : string
{
   let days = Math.floor(seconds / 86400);
   
   var hours = Math.floor(seconds / 3600);
   hours = hours % 24;
   
   var minutes = Math.floor(seconds / 60);
   minutes = minutes % 60;
   
   var leftovers = seconds % 60;
   
   var output = "";
   if (days)
   {
      output += days + "d ";
      
      if (hours < 10)
         output += "0";
   }
       
   if (days || hours)
   {
      output += hours + ":";
      
      if (minutes < 10)
         output += "0";
   }
       
   if (days || hours || minutes)  // <minutes>:<seconds>
   {
      output += minutes + ":";
      if (leftovers < 10)
         leftovers = "0" + leftovers;
   }

   output += leftovers;

   if (!days && !hours && !minutes) // only seconds?
   {
      output += 's';
   }
   return output;
}
