// @flow

/**
 * Config fetched as a raw object from the server
 */
export class ServerConfig {
   apMode: boolean;
   clientSsid: string;
   clientPassword: string;
   pingHost: string;
   pingIp: string;
   
   constructor(apMode: boolean, clientSsid: string, pingHost: string, pingIp: string)
   {
      this.apMode = apMode;
      this.clientSsid = clientSsid;
      this.clientPassword = '';
      this.pingHost = pingHost;
      this.pingIp = pingIp;
   }
}
