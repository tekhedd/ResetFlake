// @flow

import * as React from 'react';
import { action } from 'mobx';
import { observer } from 'mobx-react';

import { getStatus } from './service/status';
import { StatusData } from './lib/StatusData';
import { dhms } from './lib/util';
import { store } from './index';

type Props = {|
|};

@observer
class StatusPage extends React.Component<Props> {
   onConfig: Function;
   refresh: Function;

   constructor(props: Props) {
      super(props);
      this.onConfig = this.onConfig.bind(this);
      this.refresh = this.refresh.bind(this);
   }
   
   componentDidMount() {
      this._refresh();
   }
   
   @action
   onConfig( evt: SyntheticInputEvent<HTMLButtonElement>) {
      // evt.preventDefault();
      store.currentPage = 'config'
   }

   refresh(evt: SyntheticInputEvent<HTMLButtonElement>) {
      // evt.preventDefault();
      this._refresh();
   }
      
   _refresh() {
      store.statusMessage = "waiting for stats...";
      
      getStatus()
      .then((data : StatusData) => {
         store.statusData = data;
         store.statusMessage = "OK";
      })
      .catch((err) => {
         let msg = err.message || err.status || err;
         store.statusMessage = '' + msg;
      });
   }
   
   render() {
      let data = store.statusData;
      
      // overall status.
      
      let outage = data.outageDuration;
      
      let statusStr = data.status;
      if (outage >= 0) { // if there's a current outage
         statusStr = statusStr + ' ' + dhms(outage); // append the down time
      }

      // History
      
      var lastOutage;
      if (data.outageHistory.length <= 0) {
         lastOutage = {
            ago: 0,
            length: 0
         };
      }
      else {
         lastOutage = data.outageHistory[data.outageHistory.length - 1];
      }
      
      let lastOutageWas;
      let lastOutageLength;
      if (lastOutage.length <= 0) // never!
      {
         lastOutageWas = "NEVER";
         lastOutageLength = dhms(lastOutage.length);
      }
      else
      {
         lastOutageWas = dhms(lastOutage.ago);
         lastOutageLength = dhms(lastOutage.length);
      }

      let ping;      
      if (outage < 0) { // no current outage?
          ping = data.ping;
      }
      else {
          ping = "(FAIL)";
      }
      
      return (
      <div className="main">
         <h3>ResetFlake Stats</h3>
         
         <div className="ra-btn">
            { store.statusMessage }
            &nbsp;
            <button type="button" onClick={ this.refresh } >Refresh</button>
            <button type="button" onClick={ this.onConfig } >Config</button>
         </div>
     
         <div className="data">
            {/* -- Status -- */}
            <div className="data-block">
               <div className="lvpair status-block">
                  <label>Status</label>
                  <div>{ statusStr }</div>
               </div>
               <div className="lvpair">
                  <label>Ping</label>
                  <div>{ ping }ms</div>
               </div>
            </div>
            
            {/* -- History -- */}
            <div className="data-block">
               <div className="lvpair">
                  <label>Last outage</label>
                  <div>{ lastOutageWas }</div>
               </div>
               <div className="lvpair">
                  <label>lasting</label>
                  <div>{ lastOutageLength }</div>
               </div>
            </div>
            
            {/* -- Stats and uptime -- */}
            <div className="data-block">
               <div className="lvpair">
                  <label>Network uptime</label>
                  <div>{ dhms(data.linkUp) }</div>
               </div>
               <div className="lvpair">
                  <label>ResetFlake uptime</label>
                  <div>{ dhms(data.uptime) }</div>
               </div>
               <div className="lvpair">
                  <label>Reset count</label>
                  <div>{ data.resetCount }</div>
               </div>
            </div>
             
            {/* -- Detailed stats (ping max/avg etc) -- */}
            <div className="data-block">
               <div className="lvpair">
                  <label>Average ping</label>
                  <div>{ data.pingAvg }ms</div>
               </div>
               <div className="lvpair">
                  <label>Max ping</label>
                  <div>{ data.pingMax }ms</div>
               </div>
            </div>
            
            <div className="data-block">
               <div className="lvpair">
                  <label>Average outage length</label>
                  <div>{ dhms(data.outageAvg) }</div>
               </div>
               <div className="lvpair">
                  <label>Max outage length</label>
                  <div>{ dhms(data.outageMax) }</div>
               </div>
            </div>
         </div>
         
         <div id="footer">
            <div className="content">
               Updated { new Date().toString() }
            </div>
         </div>
      </div>
   );
   }
}

export default StatusPage;