// @flow

import * as React from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';

import { ServerConfig } from './lib/ServerConfig';
import { saveConfig } from './service/config';
import type { WifiScanResult } from './service/wifiScan';
import { getWifiScan } from './service/wifiScan';
import { store } from './index';

import './ConfigPage.css';

type Props = {|
|};

@observer
class ConfigPage extends React.Component<Props> {
   
   onHome: Function;
   onSave: Function;
   onScan: Function;
   
   @observable
   statusMsg: string = '';
   
   @observable
   scanResults: Array<WifiScanResult> = [];
   
   // Local edited values of the configuration
   
   @observable
   ssid: string = '';
   
   @observable
   password: string = '';
   
   @observable
   pingHost: string = '';
   
   @observable
   pingIp: string = '';
   
   constructor(props : Props) {
      super(props);
      
      this.onHome = this.onHome.bind(this);
      this.onSave = this.onSave.bind(this);
      this.onScan = this.onScan.bind(this);
   }
   
   componentDidMount() {
      // Get initial form values from current global config
      let config = store.config;
      
      this.ssid = config.clientSsid;
      this.password = '';
      this.pingHost = config.pingHost;
      this.pingIp = config.pingIp;
   }
   
   onHome(evt: SyntheticInputEvent<HTMLButtonElement>) {
      store.currentPage = 'status'
   }
   
   onSave(evt: SyntheticInputEvent<HTMLButtonElement>) {
      let config =
         new ServerConfig(
            true, // Hide the stats page until we're reconnected
            this.ssid,
            this.pingHost,
            this.pingIp );
      
      config.clientPassword = this.password; // if empty string, will not change
      
      saveConfig(config)
      .then(() => {
         store.config = config; // Well it's accurateish
         this.statusMsg = "ResetFlake is rebooting. You should probably refresh the page.";
      })
      .catch((err) => {
         console.error(err);
      });
   }
   
   @action
   onScan(evt: SyntheticInputEvent<HTMLButtonElement>) {
      this.statusMsg = "scanning...";
      this.scanResults = [];
      
      getWifiScan()
      .then((result: Array<WifiScanResult>) => {
         this.scanResults = result;
         let count = this.scanResults.length;
         if (count < 1) {
            this.statusMsg = "No APs found";
         }
         else {
            this.statusMsg = count + ' APs found'
         }
      })
      .catch((err) => {
         let msg = "Error while scanning: " + (err.message || err.status || err);
         this.statusMsg = msg;
         console.error(msg);
      });
   }

   render() {

      // Only render the wifi list if we have scanned
      
      let apList = null;
      let defaultOption = null;
      if (this.scanResults.length) {
         // If the current wifi name is not in the select list, include a blank
         // "default" option at the top of the list
         
         if (! this.scanResults.find((each) => each.ssid === this.ssid)) {
            defaultOption = <option value={ this.ssid }></option>;
         }
         
         apList =
            <select
               className='apList'
               size={ 8 }
               value={ this.ssid }
               onChange={ (evt) => this.ssid = evt.target.value } 
            >
               { defaultOption }
               {
                  this.scanResults.map((each: WifiScanResult) => (
                     <option value={ each.ssid }>{ each.ssid } [{ each.rssi }]</option>
                  ))
               }
            </select>;
      }
      
      return (
         <div className='wifiMain'>
            {
               // Don't render the header or "go home" button in ap mode
               ( store.config.apMode ? null : 
                  <div className="ra-btn">
                     <button type="button" onClick={ this.onHome } >Home</button>
                  </div>
               )
            }

            <div className='form'>
               <button type="button" onClick={ this.onScan } >Scan</button>
               <div className='msg'>{ this.statusMsg }</div>
               { apList }
               
               <label htmlFor="ssid">SSID</label>
               <input id='ssid' maxLength={ 32 }
                  autoFocus={ true }
                  value={ this.ssid }
                  onChange={ (evt) => this.ssid = evt.target.value }
               />
               <label htmlFor="pass">Password</label>
               <input id='pass' maxLength={ 64 } type='password'
                  value={ this.password }
                  placeholder='(required if you change SSID)'
                  onChange={ (evt) => this.password = evt.target.value }
               />
               <label htmlFor="pingH">Ping Hostname</label>
               <input id='pingH' maxLength={ 255 }
                  value={ this.pingHost }
                  onChange={ (evt) => this.pingHost = evt.target.value }
               />
               <label htmlFor="pingI">Ping IP</label>
               <input id='pingI' maxLength={ 39 }
                  value={ this.pingIp }
                  onChange={ (evt) => this.pingIp = evt.target.value }
               />
               <button
                  className="save"
                  type='button'
                  onClick={ this.onSave }
                  >Save &amp; Reboot</button>
            </div>
         </div>
      );
   }
}

export default ConfigPage;