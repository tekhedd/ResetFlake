// @flow

import React from 'react';
import ReactDOM from 'react-dom';
import url from 'url';

import GlobalStore from './GlobalStore';
import Main from './Main';

import './index.css';


export const store = new GlobalStore();

let root = document.getElementById('root');
if (!root) {
   console.error("'root' element not found!");
}
else {
   ReactDOM.render(<Main />, root);
}
