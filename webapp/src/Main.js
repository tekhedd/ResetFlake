// @flow

import * as React from 'react';
import { observer } from 'mobx-react';

import { getConfig } from './service/config';
import { ServerConfig } from './lib/ServerConfig';
import StatusPage from './StatusPage';
import ConfigPage from './ConfigPage';

import './Main.css';
import { store } from './index';

type Props = {|
|};

@observer
class Main extends React.Component<Props> {
   fetchConfig: Function;
   
   constructor(props: Props) {
      super(props);
      this.fetchConfig = this.fetchConfig.bind(this);
   }
   
   componentDidMount() {
      this.fetchConfig();
   }

   fetchConfig() {
      store.error = { message: "Fetching config..." };
      
      getConfig()
      .then((config: ServerConfig) => {
         store.config = config;
         
         // In ap mode, the status page is useless anyway.
         if (config.apMode) {
            store.currentPage = 'config';
         }
         else {
            store.currentPage = 'status';
         }
         
         // Initially displays the "loading" error message
         store.error = null;
      })
      .catch((err) => { store.error = err });
   }
   
   render() {
      let renderPage = null;
      
      let errorMessage = null;
      let err = store.error;
      if (err) {
         errorMessage = err.message || err.status || err;
         renderPage = <div className='errorMain'>{ errorMessage }</div>;
      }
      else if (store.currentPage === 'status') {
         renderPage = <StatusPage />;
      }
      else if (store.currentPage === 'config') {
         renderPage = <ConfigPage />;
      }
      else {
         console.log("Unexpected store.currentPage: " + store.currentPage);
      }
      
      return renderPage;
   }
}

export default Main;
