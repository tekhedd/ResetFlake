// @flow

import { observable } from 'mobx';
import { ServerConfig } from './lib/ServerConfig';
import { StatusData } from './lib/StatusData';

class GlobalStore {
   /**
    * Primitive error reporting: if this is nonempty, it is displayed as the app.
    * We will attempt to figure out whether it has the standard error message
    * properties.
    */
   @observable
   error: Object | null = { message: 'App Loading...' };;
   
   /**
    * Currently visible page (if the error dialog is not active.
    * Yeah we could use a router. nah.
    */
   @observable
   currentPage: ('status'|'config') = 'config';
   
   /**
    * Global status string, as in "loading" or "OK"
    */
   @observable
   statusMessage: string = 'App Loading...';
   
   /**
    * Latest status from the server
    */
   @observable
   statusData: StatusData = new StatusData();
   
   /**
    * Server config
    */
   @observable
   config: ServerConfig = new ServerConfig(false, "", "", "");
}

export default GlobalStore;