// @flow

import type { StatusData } from '../lib/StatusData';

const STATS_URL = "api/stats";

/**
 * Get current status from server
 */
export function getStatus() : Promise<StatusData> {
   return fetch(STATS_URL)
   .then((response) => {
      if (response.status >= 400) {
         return Promise.reject("Could not fetch status");
      }
      
      return response.json();
   })
   .then((json) => {
      // TODO: check that data is legit
      return json;
   });
}
