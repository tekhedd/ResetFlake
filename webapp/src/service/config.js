// @flow

import { ServerConfig } from '../lib/ServerConfig';

const URL = "api/config";

/**
 * Get current status from server
 */
export function getConfig() : Promise<ServerConfig> {
   return fetch(URL)
   .then((response) => {
      if (response.status >= 400) {
         return Promise.reject("Could not fetch config: code " + response.status);
      }
      
      return response.json();
   })
   .then((json) => {
      // TODO do we have valid data?
      return new ServerConfig(
         json['apMode'],
         json['clientSsid'],
         json['pingHost'],
         json['pingIp'] );
   });
}

export function saveConfig(config: ServerConfig) : Promise<void> {
   return fetch(URL, {
      method: "post",
      headers: {
         'Content-Type': 'application/json'
      },
      body: JSON.stringify(config)
   })
   .then((response) => {
      if (!response.ok) {
         return Promise.reject("Save failed: " + response.statusText);
      }
   });
}