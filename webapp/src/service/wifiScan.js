// @flow

const URL = "api/wifi/scan";

export type WifiScanResult = {
   rssi: number, // strength in dB (negative)
   ssid: string,
   isEncrypted: boolean
};

/**
 * Trigger a wifi scan and wait for results (could this time out?)
 *
 * Results are sorted, which might not necessarily belong in a service wrapper but
 * as I am saying in a lot of comments today, "whatever".
 */
export function getWifiScan() : Promise<Array<WifiScanResult>> {
   return fetch(URL)
   .then((response) => {
      
      if (response.ok) {
         return response.json();
      }
      
      return Promise.reject(new Error("Could not fetch wifi scan: " + response.statusText) );
   })
   .then((json: Array<Object>) => {
      if (! Array.isArray(json)) {
         return Promise.reject(new Error("wifi scan result body is not an array!"));
      }
      
      // TODO: validate each object's data
      
      // Sort by signal strength 
      
      json.sort((left: WifiScanResult, right: WifiScanResult) => {
         return right.rssi - left.rssi;
      });
      
      // Remove duplicates
      //  * Keep the strongest rssi, which is the first one in the array

      let newArray = [];
      for (let i = 0; i < json.length; i++)
      {
         let result = json[i];
         let ssid = result.ssid;
         
         let existing = newArray.find((nested: WifiScanResult) => {
            return nested.ssid === ssid;
         });
         
         if (! existing) {          // We don't already have one?
            newArray.push(result);  // Keep this one!
         }
      }
      
      return newArray;
   });
}
