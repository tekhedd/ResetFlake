
#include "Stats.h"

Stats::Stats()
{
  _systemStatus = STATUS_UNKNOWN;
  _programStartTime = now();
  _lastResetTime = _programStartTime; // as far as we know.
  _lastSuccessTime = now(); // set to now because also start of outage if currently down
  _outageStartTime = 0; // no current outage
  _resetCount = 0;
  _avgPing = 1;
  _maxPing = 1;
  _ping = 0;
  _maxOutageSec = 0;
  _avgOutageSec = 0;
}

///
/// Get current stats as json
///
String Stats::asJson()
{
  String json = "{\n";
  
  json += "\"status\":";
  switch (_systemStatus)
  {
  case STATUS_OK:         json += "\"OK\"";         break;
  case STATUS_DOWN:       json += "\"DOWN\"";       break;
  case STATUS_RESETTING:  json += "\"RESETTING\"";  break;
  case STATUS_RESET_WAIT: json += "\"RESET_WAIT\""; break;
  
  default:
  case STATUS_UNKNOWN:
    json += "\"UNKNOWN\"";
    break;
  }
  json += ",\n";

  json += "\"uptime\":";
  json += getSystemUptimeSec();
  json += ",\n";

  json += "\"outageDuration\":";
  json += getOutageDuration();
  json += ",\n";

  json += "\"linkUp\":";
  json += getUptimeSec();
  json += ",\n";

  json += "\"outageMax\":";
  json += getMaxOutageSec();
  json += ",\n";

  json += "\"outageAvg\":";
  json += getAvgOutageSec();
  json += ",\n";

  json += "\"resetCount\":";
  json += getResetCount();
  json += ",\n";
  
  json += "\"ping\":";
  json += getPingMs();
  json += ",\n";
  
  json += "\"pingAvg\":";
  json += getAvgPingMs();
  json += ",\n";
  
  json += "\"pingMax\":";
  json += getMaxPingMs();
  json += ",\n";
  
  // Outage history!
  json += "\"outageHistory\": [\n";
  int i = 0;
  while (true)
  {
    OutageHistoryElement el = _outageHistory[i];
    json += " {\n";
    json += "  \"ago\":";
    json += now() - el.start;
    json += ",\n";
    json += "  \"length\":";
    json += el.length;
    json += "\n";
    
    if (i < (HISTORY_LENGTH - 1))
    {
      json += " },\n";
    }
    else
    {
      json += " }\n";
      break;
    }
    i++;
  }
  json += "]\n";

  json += "\n}\n";

  return json;
}
  
///
/// Current outage duration or -1 if no current outage...
///
long Stats::getOutageDuration()
{
  if (_outageStartTime == 0)
    return -1; // no outage
  
  return now() - _outageStartTime;
}

long Stats::getSystemUptimeSec()
{
  return now() - _programStartTime;
}

long Stats::getUptimeSec()
{
  if (_systemStatus != STATUS_OK)
    return 0;  // we're down *now*
    
  return now() - _lastResetTime;
}
  
///
/// Register that a reset occurred
///
void Stats::logReset()
{
  ++ _resetCount;
  _systemStatus = STATUS_RESETTING;
  
  // Log stats about uptime
  time_t nowTime = now();

  // TODO: collect historical uptime stats?
  // long uptime = nowTime - _lastResetTime;
  
  // Assume here that the outage starts at the last successful ping
  _outageStartTime = _lastSuccessTime;
  _lastResetTime = nowTime;
}
  
///
/// Called when an outage ends (with a successful ping)
/// to update max outage statistics.
/// Call logOutageStart() to start an outage.
///
/// If there is not currently an outage, does nothing.
///
void Stats::logOutageEnd(time_t when)
{
  _systemStatus = STATUS_OK;
  
  if (0 == _outageStartTime) // no current outage?
    return;

  long outageDuration = when - _outageStartTime;
  
  Serial.print("Logging end of outage, duration: ");
  Serial.println(outageDuration);

  if (outageDuration <= 0)
  {
    Serial.println("outage duration < 1s, is that possible?");
  }

  if ((unsigned long)outageDuration > _maxOutageSec)
    _maxOutageSec = outageDuration;

  _avgOutageSec = _updateMovingAverage(_avgOutageSec, outageDuration);
  
  // Push all history back one here
  
  for (int i = 0; i < (HISTORY_LENGTH - 1); i++)
  {
    _outageHistory[i] = _outageHistory[i + 1];
  }
  OutageHistoryElement & hist = _outageHistory[HISTORY_LENGTH - 1];
  hist.start = _outageStartTime;
  hist.length = outageDuration;
  
  _outageStartTime = 0; // Reset current outage status
}

///
/// Logs the ping time, and also marks the end of an outage
/// if we currently have one.
///
void Stats::logPingTime(int pingMs)
{
  _ping = pingMs;
  _lastSuccessTime = now();
  
  logOutageEnd(_lastSuccessTime);

  // We expect avgPing to be < 1000ms so this is safe?
  _avgPing = _updateMovingAverage(_avgPing, pingMs);

  if (pingMs > _maxPing)
    _maxPing = pingMs;
}

///
/// Approximates the exponential moving average value, using 
/// integer math.
///
/// \return new average value
///
long Stats::_updateMovingAverage(long oldVal, long newVal)
{
  // Not sure how accurate time-decayed math is when rounded to
  // the nearest int every time. :)
  
  long avg = (oldVal * 950) + (newVal * 50); // 95% old, 5% new, * 1000
  avg = (avg + 500) / 1000;
  return avg;
}
