#ifndef __STATS_H__
#define __STATS_H__

#include <Arduino.h>
#include <Time.h>
#include <TimeLib.h> // now()

#define HISTORY_LENGTH (6)

typedef enum SystemStatusEnum
{
  STATUS_UNKNOWN, // 0, not used
  STATUS_OK,
  STATUS_DOWN,
  STATUS_RESETTING,
  STATUS_RESET_WAIT
} SystemStatus;

///
/// Stats collection wrapper.
///
/// Just leaving it all inline for this project which is extremely sloppy but w/e
///
class Stats
{
  class OutageHistoryElement
  {
  public:
    time_t start;
    unsigned long length;
    OutageHistoryElement()
    {
      start = now();
      length = 0;
    }
    
    inline const OutageHistoryElement& operator=(const OutageHistoryElement & other)
    {
      start = other.start;
      length = other.length;
      return *this;
    }
  };
  
private:
  ///
  /// Current status
  ///
  SystemStatus _systemStatus;
  
  time_t _programStartTime;
  
  ///
  /// Time of the last successful ping
  ///
  time_t _lastSuccessTime;

  ///
  /// Time the current outage started, or 0 if everything is fine
  ///
  time_t _outageStartTime;
  
  ///
  /// Time the power switch was last cycled
  ///
  time_t _lastResetTime;

  ///
  /// Count of network resets since boot. Yeah it'll eventually wrap, but
  /// if it takes >90s to do a reset, how long will that take?
  ///
  /// There may be multiple resets during a long outage so this is not the most
  /// useful statistic.
  ///
  unsigned long _resetCount;
  
  ///
  /// Exponentially weighted average ping time in ms.
  /// We don't expect to see ping times > 1s, mostly because
  /// ESP8266Ping times out in 1s. Maybe this is configurable?
  ///
  int _avgPing;

  ///
  /// ms
  ///
  int _maxPing;
  
  ///
  /// Last ping time in ms
  ///
  int _ping;

  ///
  /// Longest outage we've seen, in seconds
  ///
  unsigned long _maxOutageSec;

  ///
  /// Exponentially weighted average outage time
  ///
  unsigned long _avgOutageSec;
  
  OutageHistoryElement _outageHistory[HISTORY_LENGTH];
  
public:  
  Stats();

  inline void printToSerial()
  {
    Serial.print(asJson());
  }

  String asJson();
  long getOutageDuration();
  long getSystemUptimeSec();
  long getUptimeSec();
  
  inline unsigned long getMaxOutageSec()
  {
    return _maxOutageSec;
  }
    
  inline unsigned long getAvgOutageSec()
  {
    return _avgOutageSec;
  }

  inline unsigned long getResetCount()
  {
    return _resetCount;
  }
  
  inline int getPingMs()
  {
    return _ping;
  }

  inline int getAvgPingMs()
  {
    return _avgPing;
  }
    
  inline int getMaxPingMs()
  {
    return _maxPing;
  }

  void logReset();
  
  ///
  /// After resetting, we wait a bit before we try to ping again
  ///
  inline void logResetWait()
  {
    _systemStatus = STATUS_RESET_WAIT;
  }
  
  inline void logPingFail()
  {
    // We don't ping while resetting, so we can safely assume that the reset
    // is over:
    _systemStatus = STATUS_DOWN;
    
    // TODO: report ping fail time for impatient user (me!)
  }

  void logOutageEnd(time_t when);

  ///
  /// Logs the ping time, and also marks the end of an outage
  /// if we currently have one.
  ///
  void logPingTime(int pingMs);
  

protected:
  long _updateMovingAverage(long oldVal, long newVal);
};

#endif // __STATS_H__
