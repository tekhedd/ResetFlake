#include <DnsServer.h>
#include <Esp8266WiFi.h>

#include "IosPortalHelper.h"

IosPortalHelper::IosPortalHelper( DNSServer & dnsServer )
: _dnsServer(dnsServer),
  _apIp(192, 168, 1, 1),
  _apNetmask(255, 255, 255, 0)
{
}
   
///
/// @return false on failure/timeout
///
bool IosPortalHelper::tryStartClient( unsigned int timeoutMillis )
{
   // Try to connect to wifi using the current saved credentials, for a while.
   // how long "a while" is is application-dependent
   WiFi.mode(WIFI_STA);
   WiFi.begin();

   unsigned long start = millis();

   while (true)
   {
      if (WL_CONNECTED == WiFi.status())
         return true;                       // Hooray!
   
      if ((millis() - start) > timeoutMillis)
         return false;
      
      delay(50);
   }
   
   // not reached
}
   
void IosPortalHelper::startAP( const String & ssid, const String & password )
{
   // Append last part of the mac address to the SP name for some degree of uniqueness
   uint8_t mac[WL_MAC_ADDR_LENGTH];
   WiFi.softAPmacAddress( mac );
   char macBuffer[(WL_MAC_ADDR_LENGTH * 2) + 1];
   macBuffer[WL_MAC_ADDR_LENGTH * 2] = '\0';
   for (int i = 0; i < WL_MAC_ADDR_LENGTH; i++)
   {
      sprintf(macBuffer + (2 * i), "%02X", (int)(mac[i]));
   }
   
   String apName = ssid + "-" + macBuffer;
   Serial.println(apName);

   WiFi.disconnect();       // just in case
   WiFi.mode(WIFI_AP);      // AP only pls
   
   WiFi.softAPConfig(_apIp, _apIp, _apNetmask);
   if (!WiFi.softAP(apName.c_str(), password.c_str()))
   {
      Serial.println("softAP() failed!");
      // Yeah OK so now what?
   }
 
   _dnsServer.setErrorReplyCode(DNSReplyCode::NoError);
   _dnsServer.start(DNS_PORT, "*", _apIp);
}
   
///
/// Web server support handler - that gets the results of a wifi scan as json, suitable
/// for use in a web gui
///
String IosPortalHelper::wifiScan()
{
   int count = WiFi.scanNetworks();
   
   String json = "[\n";
   
   for (int i = 0; i < count; i++)
   {
      if (i > 0)
         json += ",";

      long rssi = WiFi.RSSI(i);
      
      String ssid = WiFi.SSID(i);
      byte encryption = WiFi.encryptionType(i);
      
      json += "{\n";
      
      json += "\"rssi\":";
      json += rssi;
      json += ",\n";
      
      json += "\"ssid\":\"";
      json += ssid;
      json += "\",\n";
      
      json += "\"isEncrypted\":";
      if (ENC_TYPE_NONE == encryption)
         json += "false";
      else
         json += "true";
      json += "\n";
      
      json += "}";
   }

   json += "]";
   return json;
}
