#ifndef __IOS_PORTAL_HELPER__
#define __IOS_PORTAL_HELPER__

#include <Arduino.h>

///
/// Internet-Of-Things helper methods, for building captive autoconfig portals using
/// ESP8266WiFi
/// 
class IosPortalHelper
{
protected:
   ///
   /// It's likely you will never actually want to change this
   ///
   const byte DNS_PORT = 53;
   
   DNSServer & _dnsServer;
   
   // More things that you could theoretically reconfigure, but you won't.
   IPAddress _apIp;
   IPAddress _apNetmask;
   
public:
   IosPortalHelper( DNSServer & dnsServer );
   static bool tryStartClient( unsigned int timeoutMillis );
   void startAP( const String & ssid, const String & password );
   static String wifiScan();
   
   inline const IPAddress & getApIp() const { return _apIp; }
};

#endif // __IOS_PORTAL_HELPER__
